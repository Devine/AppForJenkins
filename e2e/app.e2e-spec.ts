import { AppForJenkins2Page } from './app.po';

describe('app-for-jenkins2 App', function() {
  let page: AppForJenkins2Page;

  beforeEach(() => {
    page = new AppForJenkins2Page();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
